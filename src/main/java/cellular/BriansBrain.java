package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain extends GameOfLife {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }
    @Override
    public CellState getNextCell(int row, int col) {
        CellState cellState = getCellState(row, col);
        CellState currentCellState = CellState.DEAD;
        int aliveFriends = countNeighbors(row, col, CellState.ALIVE);

        if (cellState == CellState.ALIVE) {
            currentCellState = CellState.DYING;
        } else if (cellState == CellState.DEAD && aliveFriends == 2) {
            currentCellState = CellState.ALIVE;
        }
        return  currentCellState;
    }
}
